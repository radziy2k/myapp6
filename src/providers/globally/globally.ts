import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the GloballyProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GloballyProvider {

  constructor(public http: HttpClient) {
    console.log('Hello GloballyProvider Provider');
  }

  memsave(param,value){
    if(localStorage.getItem(param)){
      localStorage.removeItem(param)
    }
    localStorage.setItem(param,value);
    return localStorage.getItem(param);
  }

  memdel(param){
    if(localStorage.getItem(param)){
      localStorage.removeItem(param);
    }
    return localStorage.getItem(param)===null?'OK':'NOTOK';
  }

  memdelall(){
    try{
      localStorage.clear;
    }
    catch(e){
      console.log("memdelall() error: ",e)
    }
  }

}
