import {Injectable} from '@angular/core';
import { HttpHeaders,HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

  headers = new HttpHeaders({"Accept": 'application/json','Content-Type' : 'application/json',})

  constructor(private http: HttpClient) {
    console.log('Hello ApiProvider Provider');
  }

  //authenticate with TM IDSS
  /*
  url: http://restless-firefly-788.getsandbox.com/login
  request: {"username:"",
            password:'',
            secret_key: ''
          }
  response: {id: '',
            username: '',
            fullname: '',
            contact:'',
            token:''
          }
  */
  authenticate(username,password){
    let url = "http://restless-firefly-788.getsandbox.com/login";
    let body = {"username": username,"password": password}
    console.log("url = "+url)
    console.log("req body: ",body);
    return new Promise((resolve, reject) => {
      this.http.post(url,body,{headers: this.headers,observe: 'response'})
      .subscribe(
        res => {
          console.log("resp body: ",JSON.stringify(res.body));
          resolve(res);
        },
        err => {
          console.log("Error occured......");
          reject(err);
        }
      );
    });
  }

  //get authenticated user detail, current reservation/check-in status with detail of place & check-out history (latest 2)
  //status:-
  // 0. not reserve/no check-in
  // 1. reserved
  // 2. occupied(checked-in)
  /*
  url: http://restless-firefly-788.getsandbox.com/user/status/detail
  request: {user_id:'',
            token:'',
          }
  response: {
            "status": "success",
            "detail": {
                "id":"1",
                "status": 1
                "name": "MTM-32N-002",
                "datetime": "JAN 18, 2019 06:00",
                "expired": "JAN 18, 2019 07:00",
                "start": "JAN 18, 2019 08:30",
                "end": "JAN 18, 2019 17:30"
            },
            "history": [{
                "id":"2",
                "name": "MTM-32S-001",
                "start": "JAN 18, 2019 08:30",
                "end": "JAN 15, 2019 17:30"
            }, {
                "id":"3",
                "name": "MTM-32S-001",
                "start": "JAN 18, 2019 08:30",
                "end": "JAN 15, 2019 17:30"
            }]
        }
  */
 getCurrentUserInfo(user_id,token){
    let url = "http://restless-firefly-788.getsandbox.com/user/status/detail";
    let body = {"user_id":user_id,"token":token}
    console.log("url = "+url)
    console.log("req body: ",body);
    return new Promise((resolve, reject) => {      
      this.http.post(url,body,{headers: this.headers,observe: 'response'})
      .subscribe(
        res => {
          console.log("resp body: ",JSON.stringify(res.body));
          resolve(res);
        },
        err => {
          console.log("Error occured......");
          reject(err);
        }
      );
    });
  }

  //get list of building authorize for user
  /*
  url: http://restless-firefly-788.getsandbox.com/reserve/buildings
  request: {user_id:'',
            token:'',
          }
  response: {
           "building": [{
                "id":"1",
                "name": "MTM"
            }, {
                "id":"2",
                "name": "TMA2"
            }]
        }
  */
 getBuildings(user_id,token){
    let url = "http://restless-firefly-788.getsandbox.com/reserve/buildings";
    let body = {"user_id":user_id,"token":token}
    console.log("url = "+url)
    console.log("req body: ",body);
    return new Promise((resolve, reject) => {      
      this.http.post(url,body,{headers: this.headers,observe: 'response'})
      .subscribe(
        res => {
          console.log("resp body: ",JSON.stringify(res.body));
          resolve(res);
        },
        err => {
          console.log("Error occured......");
          reject(err);
        }
      );
    });
  }

  //get list space types of selected building
  /*
  url: http://restless-firefly-788.getsandbox.com/reserve/buildings/types
  request: {
            building_id:'',
            user_id:'',
            token:'',
          }
  response: {
           "type": [{
                "id":"1",
                "name": "Hot Desk"
            }, {
                "id":"2",
                "name": "Discussion Area"
            }, {
                "id":"3",
                "name": "Meeting Room"
            }, {
                "id":"4",
                "name": "Mini Hall"
            }, {
                "id":"5",
                "name": "Training Room"
            }
          ]
        }
  */
 getPlaceCategories(building_id,user_id,token){
  let url = "http://restless-firefly-788.getsandbox.com/reserve/buildings/types";
  let body = {"building_id":building_id,"user_id":user_id,"token":token}
  console.log("url = "+url)
  console.log("req body: ",body);
  return new Promise((resolve, reject) => {      
    this.http.post(url,body,{headers: this.headers,observe: 'response'})
    .subscribe(
      res => {
        console.log("resp body: ",JSON.stringify(res.body));
        resolve(res);
      },
      err => {
        console.log("Error occured......");
        reject(err);
      }
    );
  });
}

  //get list of sections(floor or wing) in a selected building
  /*
  url: http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections
  request: {building_id:1,
            type:1,
            user_id:'',
            token:'',
          }
  response: {
        "sections": [{
                "id":"1",
                "name": "MTM-8S"
            }, {
                "id":"2",
                "name": "MTM-28S"
            }, {
                "id":"3",
                "name": "MTM-32N"
            }, {
                "id":"4",
                "name": "MTM-32S"
            }]
        }
  */
 getSections(building_id,type_id,user_id,token){
    let url = "http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections";
    let body  = {"building_id":building_id,"type_id":type_id,"user_id":user_id,"token":token,}
    console.log("url = "+url)
    console.log("req body: ",body);
    return new Promise((resolve, reject) => {      
      this.http.post(url,body,{headers: this.headers,observe: 'response'})
      .subscribe(
        res => {
          console.log("resp body: ",JSON.stringify(res.body));
          resolve(res);
        },
        err => {
          console.log("Error occured......");
          reject(err);
        }
      );
    });
  }

  //get list of places in a selected section
  //type:-
  // 1. meeting/discussion room/area
  // 2. desk
  //status:-
  // 0. vacant
  // 1. reserved
  // 2. occupied(checked-in)
  // 3. locked/closed
  /*
  url: http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections/places
  request: {building_id:1,
            type:2,
            section_id:4,
            status:0,
            user_id:'',
            token:'',
          }
  response: {"desks": [
                {"id":"1","name":"MTM-8S-001"},
                {"id":"2","name":"MTM-8S-002"},
                {"id":"3","name":"MTM-8S-003"},
                {"id":"4","name":"MTM-8S-004"},]
        }
  */
 getPlaces(building_id,type,section_id,session_type,status,user_id,token){
    let url = "http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections/places";
    let body = {"building_id":building_id,"type":type,"section_id":section_id,"session_type":session_type,"status":status,"user_id":user_id,"token":token,}
    console.log("url = "+url)
    console.log("req body: ",body);
    return new Promise((resolve, reject) => {      
      this.http.post(url,body,{headers: this.headers,observe: 'response'})
      .subscribe(
        res => {
          console.log("resp body: ",JSON.stringify(res.body));
          resolve(res);
        },
        err => {
          console.log("Error occured......");
          reject(err);
        }
      );
    });
  }

  //get detail on the current desk/place status and check-out history (2 latest checked-out)
  /*
  url: http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections/places/place
  request: {id:1,
            user_id:'',
            token:'',
          }
  response: {
            "status": "0",
            "detail": {
                "id":"1",
                "staff_name":"",
                "staff_contact":"",
                "name": "MTM-32N-002",
                "datetime": "JAN 18, 2019 06:00",
                "expired": "JAN 18, 2019 07:00",
                "start": "JAN 18, 2019 08:30",
                "end": "JAN 18, 2019 17:30"
            },
            "history": [{
                "id":"2",
                "staff_name":"",
                "staff_contact":"",
                "name": "MTM-32S-001",
                "start": "JAN 18, 2019 08:30",
                "end": "JAN 15, 2019 17:30"
            }, {
                "id":"3",
                "staff_name":"",
                "staff_contact":"",
                "name": "MTM-32S-001",
                "start": "JAN 18, 2019 08:30",
                "end": "JAN 15, 2019 17:30"
            }]
        }
  */
 getPlace(place_id,user_id,token){
    let url = "http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections/places/place";
    let body = {"place_id":place_id,"user_id":user_id,"token":token,}
    console.log("url = "+url)
    console.log("req body: ",body);
    return new Promise((resolve, reject) => {      
      this.http.post(url,body,{headers: this.headers,observe: 'response'})
      .subscribe(
        res => {
          console.log("resp body: ",JSON.stringify(res.body));
          resolve(res);
        },
        err => {
          console.log("Error occured......");
          reject(err);
        }
      );
    });
  }

  //reserve
  /*
  url: http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections/places/place/reserve
  request: {id:1,
            user_id:'',
            token:'',
          }
  response: {"status":"success","reason":""}
  */
 reserve(place_id,user_id,token){
    let url = "http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections/places/place/reserve";
    let body = {"place_id":place_id,"user_id":user_id,"token":token,}
    console.log("url = "+url)
    console.log("req body: ",body);
    return new Promise((resolve, reject) => {      
      this.http.post(url,body,{headers: this.headers,observe: 'response'})
      .subscribe(
        res => {
          console.log("resp body: ",JSON.stringify(res.body));
          resolve(res);
        },
        err => {
          console.log("Error occured......");
          reject(err);
        }
      );
    });
  }

  //unreserve
  /*
  url: http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections/places/place/unreserve
  request: {id:1,
            user_id:'',
            token:'',
          }
  response: {"status":"success","reason":""}
  */
 unreserve(place_id,user_id,token){
    let url = "http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections/places/place/unreserve";
    let body = {"place_id":place_id,"user_id":user_id,"token":token,}
    console.log("url = "+url)
    console.log("req body: ",body);
    return new Promise((resolve, reject) => {      
      this.http.post(url,body,{headers: this.headers,observe: 'response'})
      .subscribe(
        res => {
          console.log("resp body: ",JSON.stringify(res.body));
          resolve(res);
        },
        err => {
          console.log("Error occured......");
          reject(err);
        }
      );
    });
  }

  //check in
  /*
  url: http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections/places/place/checkin
  request: {id:1,
            user_id:'',
            token:'',
          }
  response: {"status":"success","reason":""}
  */
 checkin(place_id,user_id,token){
    let url = "http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections/places/place/checkin";
    let body = {"place_id":place_id,"user_id":user_id,"token":token,}
    console.log("url = "+url)
    console.log("req body: ",body);
    return new Promise((resolve, reject) => {      
      this.http.post(url,body,{headers: this.headers,observe: 'response'})
      .subscribe(
        res => {
          console.log("resp body: ",JSON.stringify(res.body));
          resolve(res);
        },
        err => {
          console.log("Error occured......");
          reject(err);
        }
      );
    });
  }

  //check out
  /*
  url: http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections/places/place/checkout
  request: {id:1,
            user_id:'',
            token:'',
          }
  response: {"status":"success","reason":""}
  */
 checkout(place_id,user_id,token){
    let url = "http://restless-firefly-788.getsandbox.com/reserve/buildings/types/sections/places/place/checkout";
    let body = {"place_id":place_id,"user_id":user_id,"token":token,}
    console.log("url = "+url)
    console.log("req body: ",body);
    return new Promise((resolve, reject) => {      
      this.http.post(url,body,{headers: this.headers,observe: 'response'})
      .subscribe(
        res => {
          console.log("resp body: ",JSON.stringify(res.body));
          resolve(res);
        },
        err => {
          console.log("Error occured......");
          reject(err);
        }
      );
    });
  }

  //get list of staff based on search query
  /*
  url: http://restless-firefly-788.getsandbox.com/findPeople
  request: {name:'',
            staff_id:'',
            user_id:'',
            token:'',
          }
  response: {"people": [
                {"id":"1","name":"Abu","staff_id":"S11111"},
                {"id":"2","name":"Ali","staff_id":"S22222"},
                {"id":"3","name":"Ahmad","staff_id":"S33333"},
                {"id":"4","name":"Mohd","staff_id":"S44444"},
                ]
        }
  */
 findPeople(name,staff_id,user_id,token){
    let url = "http://restless-firefly-788.getsandbox.com/findPeople";
    let body = {"name":name,"staff_id":staff_id,"user_id":user_id,"token":token,}
    console.log("url = "+url)
    console.log("req body: ",body);
    return new Promise((resolve, reject) => {      
      this.http.post(url,body,{headers: this.headers,observe: 'response'})
      .subscribe(
        res => {
          console.log("resp body: ",JSON.stringify(res.body));
          resolve(res);
        },
        err => {
          console.log("Error occured......");
          reject(err);
        }
      );
    });
  }

  //get staff info, his current & historical places (latest 2) detail
  /*
  url: http://restless-firefly-788.getsandbox.com/findPeople/detail
  request: {id:'',
            user_id:'',
            token:'',
          }
  response: {"detail": {
                "id":"1",
                "name":"Abu"
                "staff_id":"S11111"
                "contact":"011111111111"
                "status": "2",
                "space_name": "MTM-32N-003",
                "datetime": "JAN 18, 2019 07:01",
                "start": "JAN 18, 2019 08:30",
                "end": "JAN 18, 2019 17:30"
            },
            "history": [{
                "id":"2",
                "name": "MTM-32S-001",
                "start": "JAN 18, 2019 08:30",
                "end": "JAN 15, 2019 17:30"
            }, {
                "id":"3",
                "name": "MTM-32S-001",
                "start": "JAN 18, 2019 08:30",
                "end": "JAN 15, 2019 17:30"
            }]
        }
  */
 findPeopleLocation(found_user_id,user_id,token){
    let url = "http://restless-firefly-788.getsandbox.com/findPeople/detail";
    let body = {"found_user_id":found_user_id,"user_id":user_id,"token":token,}
    console.log("url = "+url)
    console.log("req body: ",body);
    return new Promise((resolve, reject) => {      
      this.http.post(url,body,{headers: this.headers,observe: 'response'})
      .subscribe(
        res => {
          console.log("resp body: ",JSON.stringify(res.body));
          resolve(res);
        },
        err => {
          console.log("Error occured......");
          reject(err);
        }
      );
    });
  }

  //generic//core
  // private postData() {
  //   console.log("URL-\n"+this.url)
  //   console.log("REQ:-\n"+JSON.stringify(this.param))
  //   return new Promise((resolve, reject) => {      

  //     this.http.post(this.url,
  //       this.param.body,
  //             {headers: new HttpHeaders({"Accept": 'application/json','Content-Type' : 'application/json',}),observe: 'response'}
  //             )
  //     .subscribe(
  //       res => {
  //         console.log("RESP:-\n"+res.body);
  //         resolve(res);
  //       },
  //       err => {
  //         console.log("RESP error occured......");
  //         console.log(err)
  //         reject(err);
  //       }
  //     );
  //   });

  // }

  // postData2(params) {
  //   console.log("credentials.url="+params.url)
  //   console.log("credentials.body="+JSON.stringify(params.body))
  //   return new Promise((resolve, reject) => {      

  //     this.http.post(params.url,
  //         params.body,
  //             //{"username": credentials.username,"password": credentials.password}, 
  //             {headers: new HttpHeaders({"Accept": 'application/json','Content-Type' : 'application/json',}),observe: 'response'}
  //             )
  //     .subscribe(
  //       res => {
  //         console.log(res.body);
  //         // console.log(res.headers);
  //         resolve(res);
  //       },
  //       err => {
  //         console.log("Error occured......");
  //         reject(err);
  //       }
  //     );
  //   });

  // }

}
