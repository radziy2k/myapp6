import { Component, ViewChild } from '@angular/core';
import { Nav, Platform,NavController,Keyboard } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { WelcomePage } from '../pages/public/welcome/welcome';
// import { ListPage } from '../pages/list/list';
import { AboutPage } from '../pages/private/about/about';
import { FindPeoplePage } from '../pages/private/findPeople/find/find';
// import { FindPeopleDetailPage } from '../pages/private/findPeople/people/people';
import { HomePage } from '../pages/private/home/home';
import { BuildingsPage } from '../pages/private/reservation/buildings/buildings';
// import { TypesPage } from '../pages/private/reservation/buildings/types/types';
// import { CategoriesPage } from '../pages/private/reservation/categories/categories';
// import { CheckinPage } from '../pages/private/reservation/checkin/checkin';
// import { CheckoutPage } from '../pages/private/reservation/checkout/checkout';
// import { PlacePage } from '../pages/private/reservation/place/place';
// import { PlacesPage } from '../pages/private/reservation/places/places';
// import { ReservePage } from '../pages/private/reservation/reserve/reserve';
// import { UnreservePage } from '../pages/private/reservation/unreserve/unreserve';
import { LoginPage } from '../pages/public/login/login';
import { App } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // @ViewChild(Nav) nav: Nav;

  rootPage: any = WelcomePage;
  fullname: any;
  userData: any;

  pages: Array<{title: string, component: any,icon:string}>;

  constructor(public platform: Platform, 
              public statusBar: StatusBar, 
              public splashScreen: SplashScreen,
              public alertCtrl: AlertController,
              public app: App,
              public keyboard:Keyboard,
              ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage,icon:'home' },
      { title: 'About', component: AboutPage,icon:'bulb' },
      { title: 'Reserve', component: BuildingsPage,icon:'bookmarks' },
      { title: 'Find Staff', component: FindPeoplePage,icon:'contacts'},
      { title: 'Logout', component: LoginPage,icon:'exit'},
    ];

    platform.ready().then(() => {
      this.statusBar.backgroundColorByHexString("#68272d"); // change color
      this.statusBar.styleBlackTranslucent();
      platform.registerBackButtonAction(() => {
        this.customBackButtonHandler();
      }, 100)
    });

  }

  customBackButtonHandler() {
    // console.log("active==="+this.nav.getActive.name)
  }

  private initializeApp() {

    this.platform.ready().then(() => {
      this.userData = JSON.parse(localStorage.getItem('userData'));
      // this.storage.get('userData').then((data) => {
      //   if(data)
          // this.fullname = userData.fullname;
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        this.statusBar.styleDefault();
        this.splashScreen.hide();
      // });
    })
  }

  openPage(page) {
    console.log("page=["+page.component.name+"]")
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    // if(page.component.name==="LoginPage")
    //   this.app.getActiveNav().setRoot(page,{message:"You are successfully logout!"}); 
    //   // this.nav.setRoot('LoginPage',{message:"You are successfully logout!"});
    // else
      // this.nav.setRoot(page.component);
      // this.app.getActiveNav().setRoot(page); 
    if(page.component.name==="LoginPage"){
      this.app.getRootNav().setRoot(page.component,{message:"You are successfully logout!"});
      localStorage.clear();
    }
    else
      this.app.getRootNav().setRoot(page.component);
    }

}
