import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { WelcomePage } from '../pages/public/welcome/welcome';
import { LoginPage } from '../pages/public/login/login';
import { HomePage } from '../pages/private/home/home';
import { AboutPage } from '../pages/private/about/about';
import { BuildingsPage } from '../pages/private/reservation/buildings/buildings';
import { CategoriesPage } from '../pages/private/reservation/buildings/categories/categories';
import { SectionsPage } from '../pages/private/reservation/buildings/categories/sections/sections';
import { PlacesPage } from '../pages/private/reservation/buildings/categories/sections/places/places';
import { PlaceMapPage } from '../pages/private/reservation/buildings/categories/sections/places/place-map/place-map';
import { PlacePage } from '../pages/private/reservation/buildings/categories/sections/places/place/place';
import { ReservePage } from '../pages/private/reservation/reserve/reserve';
import { UnreservePage } from '../pages/private/reservation/unreserve/unreserve';
import { CheckinPage } from '../pages/private/reservation/checkin/checkin';
import { QrscannerPage} from '../pages/private/reservation/qrscanner/qrscanner'
import { CheckoutPage } from '../pages/private/reservation/checkout/checkout';
import { FindPeoplePage } from '../pages/private/findPeople/find/find';
import { FindPeopleDetailPage } from '../pages/private/findPeople/people/people';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiProvider } from '../providers/api/api';

import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { QRScanner } from '@ionic-native/qr-scanner';
import { GloballyProvider } from '../providers/globally/globally';
// import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Keyboard } from '@ionic-native/keyboard';

@NgModule({
  declarations: [
    MyApp,
    WelcomePage,
    LoginPage,
    HomePage,
    AboutPage,
    BuildingsPage,
    CategoriesPage,
    SectionsPage,
    PlacesPage,
    PlaceMapPage,
    PlacePage,
    ReservePage,
    UnreservePage,
    CheckinPage,
    QrscannerPage,
    CheckoutPage,
    FindPeoplePage,
    FindPeopleDetailPage,
      ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()  
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    WelcomePage,
    LoginPage,
    HomePage,
    AboutPage,
    BuildingsPage,
    CategoriesPage,
    SectionsPage,
    PlacesPage,
    PlaceMapPage,
    PlacePage,
    ReservePage,
    UnreservePage,
    CheckinPage,
    QrscannerPage,
    CheckoutPage,
    FindPeoplePage,
    FindPeopleDetailPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    QRScanner,
    GloballyProvider,
    Keyboard,
    // BarcodeScanner
  ]
})
export class AppModule {}
