import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UnreservePage } from './unreserve';

@NgModule({
  declarations: [
    UnreservePage,
  ],
  imports: [
    IonicPageModule.forChild(UnreservePage),
  ],
})
export class UnreservePageModule {}
