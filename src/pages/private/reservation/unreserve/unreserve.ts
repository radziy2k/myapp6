import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController } from 'ionic-angular';
import {ApiProvider} from "../../../../providers/api/api";
import { Storage } from '@ionic/storage';
import { App } from 'ionic-angular';
import { HomePage } from '../../home/home';

/**
 * Generated class for the UnreservePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-unreserve',
  templateUrl: 'unreserve.html',
})
export class UnreservePage {
  detail={id:0,name:'',datetime:'',expired:'',start:'',end:''};

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public api:ApiProvider,
              public storage:Storage,
              public app: App,
              public menuCtrl: MenuController,
              ) {
    console.log('constructor UnreservePage');
    this.menuCtrl.enable(false, 'menu');

    this.storage.get('userBookingData').then((data) => {
      this.detail=data;
      console.log("this.detail",this.detail)
      console.log("id",this.detail.id)
      console.log("name",this.detail.name)
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UnreservePage');
  }

  proceed(){
    console.log('proceed UnreservePage'); 
    console.log('unreserved id',this.detail.id); 
    this.app.getRootNav().setRoot(HomePage,{message:"You are successfully release a reservation!"});
  }

}
