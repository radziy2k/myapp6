import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController,ToastController } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import {ApiProvider} from "../../../../providers/api/api";
// import { Storage } from '@ionic/storage';
import { App } from 'ionic-angular';
import { HomePage } from '../../home/home';
import {QrscannerPage} from '../qrscanner/qrscanner'

/**
 * Generated class for the CheckinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkin',
  templateUrl: 'checkin.html',
})
export class CheckinPage {

  userData : any;
  responseData: any;
  scannedtext='';
  status:any;
  detail= {name:"MTM-32S-090",id:"18",status:"1",
          staff_name:"Abu Ali",staff_contact:"013333333",
          datetime:"JAN 18, 2019 06:00",expired:"JAN 18, 2019 07:00",
          start:"JAN 18, 2019 08:30",end:"JAN 18, 2019 17:30"
};

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public api:ApiProvider,
              // public storage:Storage,
              public app: App,
              public menuCtrl: MenuController,
              private toastCtrl:ToastController,
              private qrScanner: QRScanner) {
    this.userData = localStorage.getItem('userData');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckinPage');
    this.scannedtext = localStorage.getItem('SCANTEXT');
    console.log("SCANNED TEXT:",this.scannedtext)

    if(this.scannedtext){
      console.log("SCANNED TEXT:",this.scannedtext)
      //cross check SCANTEXT with API
      this.api.getPlace(this.scannedtext,this.userData.id,this.userData.token).then((result) =>{
        
        this.responseData = result;

        console.log("this.responseData.body.detail: ",this.responseData.body.detail)

        // this.status = this.responseData.body.detail.status;
        
        this.detail.name=this.responseData.body.name;
        this.detail.id=this.responseData.body.id;

        switch(this.responseData.body.status){
          case '0':
            this.detail.status="Vacant"
            break;
          case '1':
            this.detail.status="Reserved";
            break;
          case '2':
            this.detail.status="Checked-In";
            break;
          default:
            this.detail.status="Closed";
        }

        this.detail.staff_name=this.responseData.body.staff_name;
        this.detail.staff_contact=this.responseData.body.staff_contact;
        this.detail.datetime=this.responseData.body.datetime;
        this.detail.expired=this.responseData.body.expired;
        this.detail.start=this.responseData.body.start;
        this.detail.end=this.responseData.body.end;
    
        console.log("place detail:",JSON.stringify(this.detail))

    }, (err) => {
        //Connection failed message
        this.presentToast("Connection issue, please retry later!",10000);
      });
    }
    else
      console.log("scannedtext is null or empty:",this.scannedtext)

    console.log('ionViewDidLoad ended');
  }

  goscan() {
    this.navCtrl.push(QrscannerPage);
  }

  presentToast(msg,duration) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: 'bottom',
      dismissOnPageChange: true,
      cssClass: 'mytoast'
    });
    toast.present();
  }

}
