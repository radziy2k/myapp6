import { Component } from '@angular/core';
import { IonicPage, LoadingController,NavController, NavParams,ToastController,MenuController } from 'ionic-angular';
import {ApiProvider} from "../../../../../../../../providers/api/api";

/**
 * Generated class for the PlaceMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-place-map',
  templateUrl: 'place-map.html',
})
export class PlaceMapPage {

  userData : any;// {id:"",username:"",fullname:"",contact:"",token:""};  
  responseData: any;
  rows=[];


  building_id:any;
  building_name:any;
  type_id: any;
  type_name:any;
  section_id:any;
  section_name:any;
  status:any;

  constructor(public navCtrl: NavController, 
              public menuCtrl: MenuController,
              public navParams: NavParams,
              public api: ApiProvider,
              private toastCtrl:ToastController,
              public loadingController: LoadingController,
              // private storage:Storage
              ) {

    console.log("constructor PlaceMapPage()") 

    this.building_id = navParams.get('building_id')
    this.building_name = navParams.get('building_name')
    this.type_id = navParams.get('type_id')
    this.type_name = navParams.get('type_name')
    this.section_id = navParams.get('section_id')
    this.section_name = navParams.get('section_name')

    this.status = localStorage.getItem("placesStatus");
    console.log("building_id=",this.building_id)
    console.log("building_name=",this.building_name)
    console.log("type_id=",this.type_id)
    console.log("type_name=",this.type_name)
    console.log("section_id=",this.section_id)
    console.log("section_name=",this.status)
    if(!this.status){
      this.status = navParams.get('status')
    }
    console.log("this.status=[",this.status)

    this.menuCtrl.enable(false, 'menu');
  }
}
