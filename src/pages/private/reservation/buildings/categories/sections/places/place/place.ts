import { Component, HostListener, Output, EventEmitter, ElementRef, Input } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,MenuController,LoadingController } from 'ionic-angular';

import {ApiProvider} from "../../../../../../../../providers/api/api";
import {GloballyProvider} from "../../../../../../../../providers/globally/globally";
import { Keyboard } from '@ionic-native/keyboard';

/**
 * Generated class for the PlacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-place',
  templateUrl: 'place.html',
})
export class PlacePage {

  userData : any;// {id:"",username:"",fullname:"",contact:"",token:""};  

  responseData: any;

  status: any;//0=empty,1=reserve,2=checkin

  detail= {id:0,name:"",datetime:"",start:"",end:"",staff_id:"",staff_name:"",staff_contact:""};

  history: any;
  header: any;

  reserveButton: any;
  releaseReserveButton: any;
  checkinButton: any;
  checkoutButton: any;

  popupmessage : any;
  message: any;

  place_id:any;
  building_id:any;
  building_name:any;
  type_id:any;
  type_name:any;
  section_id:any;
  section_name:any;
  session_type:any;
  session_type_name:any;


  constructor(public navCtrl: NavController, 
              public menuCtrl: MenuController,
              public api: ApiProvider,
              private toastCtrl:ToastController,
              public navParams: NavParams,
              public g:GloballyProvider,
              public Keyboard: Keyboard,
              public elementRef: ElementRef,
              public loadingController: LoadingController){
      console.log("constructor") 

      this.place_id = navParams.get('place_id')
      this.building_id = navParams.get('building_id')
      this.building_name = navParams.get('building_name')
      this.type_id = navParams.get('type_id')
      this.type_name = navParams.get('type_name')
      this.section_id = navParams.get('section_id')
      this.section_name = navParams.get('section_name')
      this.session_type = navParams.get('session_type')

      switch(this.session_type){
        case 1:
          this.session_type_name = "8.30 am - 1.00 pm (Morning)"
          break;
        case 2:
          this.session_type_name = "2.00 pm - 5.30 pm (Afternoon)"
          break;
        default:
          this.session_type_name = "8.30 am - 5.30 pm (Full day)"
      }
    
      this.status = localStorage.getItem("placesStatus");
      console.log("place_id=",this.place_id)
      console.log("building_id=",this.building_id)
      console.log("building_name=",this.building_name)
      console.log("type_id=",this.type_id)
      console.log("type_name=",this.type_name)
      console.log("section_id=",this.section_id)
  
      this.menuCtrl.enable(false, 'menu');
            
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlacePage');
    let loading = this.loadingController.create({content : "Please wait..."});
    loading.present();

    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.api.getPlace(this.place_id,this.userData.id,this.userData.token).then((result) =>{
      
      this.responseData = result;
      this.status = this.responseData.body.status;
      this.history = this.responseData.body.history;
      
      this.detail.name=this.responseData.body.detail.name;
      this.detail.staff_name=this.responseData.body.staff_name;
      this.detail.staff_contact=this.responseData.body.staff_contact;
      this.detail.datetime=this.responseData.body.detail.datetime;
      this.detail.start=this.responseData.body.detail.start;
      this.detail.end=this.responseData.body.detail.end;
  
      if(this.status==="0"){//if empty
        this.reserveButton=true;
        this.releaseReserveButton=false;
        this.checkinButton=true;
        this.checkoutButton=false;
        if(!this.message)
          this.message="";
      }
      else{//if checked-in
        this.reserveButton=false;
        this.releaseReserveButton=false;
        this.checkinButton=false;
        this.checkoutButton=true;
        if(!this.message)
          this.message="Already have occupant!";
      }
      console.log(this.message)
      loading.dismissAll();
    }, (err) => {
      //Connection failed message
      this.presentToast("Connection issue, please retry later!",10000);
      loading.dismissAll();
    });
  }

  presentToast(msg,duration) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: 'bottom',
      dismissOnPageChange: true,
      cssClass: 'mytoast'
    });
    toast.present();
  }

}
