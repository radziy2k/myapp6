import { Component } from '@angular/core';
import { IonicPage, LoadingController,NavController, NavParams,ToastController,MenuController } from 'ionic-angular';
import {ApiProvider} from "../../../../../../../providers/api/api";
// import { Storage } from '@ionic/storage';

import { PlaceMapPage } from './place-map/place-map';
import { PlacePage } from './place/place';

/**
 * Generated class for the BuildingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-places',
  templateUrl: 'places.html',
})
export class PlacesPage {
  userData : any;// {id:"",username:"",fullname:"",contact:"",token:""};  
  responseData: any;
  rows=[];


  building_id:any;
  building_name:any;
  type_id: any;
  type_name:any;
  section_id:any;
  section_name:any;
  session_type:any;
  session_type_name:any;
  status:any;

  constructor(public navCtrl: NavController, 
              public menuCtrl: MenuController,
              public navParams: NavParams,
              public api: ApiProvider,
              private toastCtrl:ToastController,
              public loadingController: LoadingController,
              // private storage:Storage
              ) {

    console.log("constructor") 

    this.building_id = navParams.get('building_id')
    this.building_name = navParams.get('building_name')
    this.type_id = navParams.get('type_id')
    this.type_name = navParams.get('type_name')
    this.section_id = navParams.get('section_id')
    this.section_name = navParams.get('section_name')
    this.session_type = navParams.get('session_type')

    switch(this.session_type){
      case 1:
        this.session_type_name = "8.30 am - 1.00 pm (Morning)"
        break;
      case 2:
        this.session_type_name = "2.00 pm - 5.30 pm (Afternoon)"
        break;
      default:
        this.session_type_name = "8.30 am - 5.30 pm (Full day)"
    }

    this.status = localStorage.getItem("placesStatus");
    console.log("building_id=",this.building_id)
    console.log("building_name=",this.building_name)
    console.log("type_id=",this.type_id)
    console.log("type_name=",this.type_name)
    console.log("section_id=",this.section_id)
    console.log("section_name=",this.status)
    if(!this.status){
      this.status = navParams.get('status')
    }
    console.log("this.status=[",this.status)

    this.menuCtrl.enable(false, 'menu');
  }

  onChange(value){
    localStorage.setItem("placesStatus",value)
    console.log("onChange:",value);
    // this.navCtrl.setRoot(this.navCtrl.getActive().component);
    this.ionViewDidLoad();
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad');

    let loading = this.loadingController.create({content : "Please wait..."});
    loading.present();

    this.userData = localStorage.getItem("userData");
    this.api.getPlaces(this.building_id,this.type_id,this.section_id,this.session_type,this.status,this.userData.id,this.userData.token).then((result) =>{
        
      this.responseData = result;
      let length = this.responseData.body.desks.length;

      this.rows = [];
      for (let i = 0; i < length; i += 4) {
        let column = [];
        column.push(this.responseData.body.desks[i]);
        if (i + 1 < length) {
          column.push(this.responseData.body.desks[i + 1]);
        }
        if (i + 2 < length) {
          column.push(this.responseData.body.desks[i + 2]);
        }
        if (i + 3 < length) {
          column.push(this.responseData.body.desks[i + 3]);
        }
        this.rows.push(column);
      }

      loading.dismissAll();
    }, (err) => {
        //Connection failed message
        this.presentToast("Connection issue, please retry later!",10000);
        loading.dismissAll();
      });
  }

  gotoPlacePage(place_id){
      this.navCtrl.push(PlacePage, {
        place_id: place_id,
        building_id: this.building_id,
        building_name: this.building_name,
        type_id: this.type_id,
        type_name: this.type_name,
        section_id: this.section_id,
        section_name: this.section_name,
      });
  }  

  gotoViewMap(building,id,section_id){
    this.navCtrl.push(PlaceMapPage, {
      building_id: this.building_id,
      building_name: this.building_name,
      type_id: this.type_id,
      type_name: this.type_name,
      section_id: this.section_id,
      section_name: this.section_name,
    });
  }

  presentToast(msg,duration) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: 'bottom',
      dismissOnPageChange: true,
      cssClass: 'mytoast'
    });
    toast.present();
  }

}
