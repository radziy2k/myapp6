import { Component } from '@angular/core';
import { IonicPage, LoadingController,NavController, NavParams,ToastController,MenuController } from 'ionic-angular';
import {ApiProvider} from "../../../../../providers/api/api";
// import { Storage } from '@ionic/storage';

import { SectionsPage } from './sections/sections';

/**
 * Generated class for the BuildingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage {
  userData : any;// {id:"",username:"",fullname:"",contact:"",token:""};  
  responseData: any;
  types: any;
  building_id:any;
  building_name:any;
  session_type:any;
  session_type_name: any;

  constructor(public navCtrl: NavController, 
              public menuCtrl: MenuController,
              public navParams: NavParams,
              public api: ApiProvider,
              private toastCtrl:ToastController,
              public loadingController: LoadingController,
              // private storage:Storage
              ) {

    console.log("constructor") 

    this.building_id = navParams.get('building_id')
    this.building_name = navParams.get('building_name')
    this.session_type = navParams.get('session_type')
    switch(this.session_type){
      case 1:
        this.session_type_name = "8.30 am - 1.00 pm (Morning)"
        break;
      case 2:
        this.session_type_name = "2.00 pm - 5.30 pm (Afternoon)"
        break;
      default:
        this.session_type_name = "8.30 am - 5.30 pm (Full day)"
    }

    console.log("session_type:",this.session_type)
    console.log("session_type_name:",this.session_type_name)

    this.menuCtrl.enable(false, 'menu');
  }

  refreshme(){
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
    }

  ionViewDidLoad() {      
    console.log('ionViewDidLoad');

    let loading = this.loadingController.create({content : "Please wait..."});
    loading.present();

    this.userData = localStorage.getItem("userData");
    this.api.getPlaceCategories(this.building_id,this.userData.id,this.userData.token).then((result) =>{
        
      this.responseData = result;
      this.types = this.responseData.body.types

      loading.dismissAll();
    }, (err) => {
        //Connection failed message
        this.presentToast("Connection issue, please retry later!",10000);
        loading.dismissAll();
      });
  }

  gotoSectionsPage(type_id,type_name){
      this.navCtrl.push(SectionsPage, {
        building_id: this.building_id,
        building_name: this.building_name,
        session_type: this.session_type,
        type_id: type_id,
        type_name: type_name,
      });
  }  

  presentToast(msg,duration) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: 'bottom',
      dismissOnPageChange: true,
      cssClass: 'mytoast'
    });
    toast.present();
  }

}
