import { Component } from '@angular/core';
import { IonicPage, LoadingController,NavController, NavParams,ToastController,MenuController } from 'ionic-angular';
import {ApiProvider} from "../../../../providers/api/api";
// import { Storage } from '@ionic/storage';

import { CategoriesPage } from './categories/categories';

/**
 * Generated class for the BuildingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-buildings',
  templateUrl: 'buildings.html',
})
export class BuildingsPage {
  userData : any;// {id:"",username:"",fullname:"",contact:"",token:""};  
  // userBookingData: any;

  responseData: any;

  status: any;//0=empty,1=reserve,2=checkin

  building: any;
  session_type: any;

  header: any;

  reserveButton: any;
  releaseReserveButton: any;
  checkinButton: any;
  checkoutButton: any;

  popupmessage : any;
  message: any;

  constructor(public navCtrl: NavController, 
              public menuCtrl: MenuController,
              public navParams: NavParams,
              public api: ApiProvider,
              private toastCtrl:ToastController,
              public loadingController: LoadingController,
              // private storage:Storage
              ) {

    console.log("constructor") 

    this.message = navParams.get('message')
    this.popupmessage = navParams.get('popupmessage')
    this.menuCtrl.enable(true, 'menu');

  }

  updateSessionType(type){
    this.session_type = type;
    console.log("session type:",this.session_type)
  }

  refreshme(){
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
    }

  ionViewDidLoad() {      
    console.log('ionViewDidLoad');
    this.session_type = "3"

    let loading = this.loadingController.create({content : "Please wait..."});
    loading.present();

    this.userData = localStorage.getItem("userData");
    this.api.getBuildings(this.userData.id,this.userData.token).then((result) =>{
        
      this.responseData = result;
      this.building = this.responseData.body.building

      loading.dismissAll();
    }, (err) => {
        //Connection failed message
        this.presentToast("Connection issue, please retry later!",10000);
        loading.dismissAll();
      });
  }

  gotoCategoriesPage(building_id,building_name){
    console.log("session_type:",this.session_type)
    this.navCtrl.push(CategoriesPage, {
      building_id: building_id,
      building_name: building_name,
      session_type: this.session_type,
    });
}  

  presentToast(msg,duration) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: 'bottom',
      dismissOnPageChange: true,
      cssClass: 'mytoast'
    });
    toast.present();
  }

}
