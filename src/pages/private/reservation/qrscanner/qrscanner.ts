import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { CheckinPage } from '../checkin/checkin';

@Component({
  selector: 'page-qrscanner',
  templateUrl: 'qrscanner.html',
})
export class QrscannerPage {

  constructor(public navCtrl: NavController,
              public platform:Platform,
              public qrScanner: QRScanner) {
                
                // solve the problem - "plugin not installed".
                platform.ready().then(()=>{
                  this.qrscanner();
                })
  }

  qrscanner() {
    
    // Optionally request the permission early
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted
          console.log('authorized');
          let element: any = document.getElementsByClassName('menu-content');

          console.log("element length:",element.length);
          console.log("bgcolor:",element[0].style.background);

          window.document.querySelector('ion-app').classList.add('transparentBody')
          console.log("className:",window.document.querySelector('ion-app').className)

          // start scanning
          let scanSub = this.qrScanner.scan().subscribe((text: string) => {
            console.log('Scanned something', text);
            // alert(text);
            localStorage.removeItem('SCANTEXT');
            localStorage.setItem('SCANTEXT', text)
            this.qrScanner.hide(); // hide camera preview
            scanSub.unsubscribe(); // stop scanning
            this.navCtrl.pop();
          });

          console.log('this.qrScanner.resumePreview before');
          this.qrScanner.resumePreview();
          console.log('this.qrScanner.resumePreview after');

          // show camera preview
          this.qrScanner.show()
          .then((data : QRScannerStatus)=> { 
            // alert(data.showing);
            console.log("data.showing:",data.showing)
          },err => {
            this.ionViewDidLeave();
            console.log("err:",err)
          });
          console.log('this.qrScanner.show');

          // wait for user to scan something, then the observable callback will be called

        } else if (status.denied) {
          // alert('denied');
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
          // alert('else');
        }
      })
      .catch((e: any) => {
        alert('Error is' + e);
      });

  }

  ionViewDidLeave() {
    console.log("ionViewDidLeave")
    window.document.querySelector('ion-app').classList.remove('transparentBody')
    this.navCtrl.push(CheckinPage)
  }

}
