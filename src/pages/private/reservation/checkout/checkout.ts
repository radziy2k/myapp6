import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController } from 'ionic-angular';
import {ApiProvider} from "../../../../providers/api/api";
import { Storage } from '@ionic/storage';
import { App } from 'ionic-angular';
import { HomePage } from '../../home/home';

/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {
  detail={id:0,name:'',datetime:'',expired:'',start:'',end:''};

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public storage:Storage,
              public app: App,
              public menuCtrl: MenuController,
) {
  this.menuCtrl.enable(false, 'menu');

  this.storage.get('userBookingData').then((data) => {
    this.detail=data;
    console.log("this.detail",this.detail)
    console.log("id",this.detail.id)
    console.log("name",this.detail.name)
  })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutPage');
  }

  proceed(){
    console.log('proceed CheckoutPage'); 
    console.log('checkout id',this.detail.id); 
    this.app.getRootNav().setRoot(HomePage,{message:"You are successfully check-out!"});
  }


}
