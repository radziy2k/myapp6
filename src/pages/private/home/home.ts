import { Component } from '@angular/core';
import { IonicPage, LoadingController,NavController, NavParams,ToastController,MenuController } from 'ionic-angular';
import {ApiProvider} from "../../../providers/api/api";
// import { Storage } from '@ionic/storage';

import { BuildingsPage } from '../reservation/buildings/buildings';
import { UnreservePage } from '../reservation/unreserve/unreserve';
import { CheckinPage } from '../reservation/checkin/checkin';
import { CheckoutPage } from '../reservation/checkout/checkout';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  userData : any;// {id:"",username:"",fullname:"",contact:"",token:""};  
  // userBookingData: any;

  responseData: any;

  status: any;//0=empty,1=reserve,2=checkin

  detail= {id:0,name:"",datetime:"",expired:"",start:"",end:""};

  history: any;
  header: any;

  reserveButton: any;
  releaseReserveButton: any;
  checkinButton: any;
  checkoutButton: any;

  popupmessage : any;
  message: any;

  constructor(public navCtrl: NavController, 
              public menuCtrl: MenuController,
              public navParams: NavParams,
              public api: ApiProvider,
              private toastCtrl:ToastController,
              public loadingController: LoadingController,
              // private storage:Storage
              ) {

    console.log("constructor") 

    this.message = navParams.get('message')
    this.popupmessage = navParams.get('popupmessage')
    this.menuCtrl.enable(true, 'menu');
  }

  refreshme(){
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
    }

  ionViewDidLoad() {      
    console.log('ionViewDidLoad');

    let loading = this.loadingController.create({content : "Please wait..."});
    loading.present();

    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.api.getCurrentUserInfo(this.userData.id,this.userData.token).then((result) =>{
      
      this.responseData = result;
      this.status = this.responseData.body.detail.status;
      this.history = this.responseData.body.history;
      
      this.detail.name=this.responseData.body.detail.name;
      this.detail.datetime=this.responseData.body.detail.datetime;
      this.detail.expired=this.responseData.body.detail.expired;
      this.detail.start=this.responseData.body.detail.start;
      this.detail.end=this.responseData.body.detail.end;

      localStorage.removeItem('userBookingData')
      localStorage.setItem('userBookingData',JSON.stringify(this.detail))
  
      if(this.status==="0"){//if empty
        this.reserveButton=true;
        this.releaseReserveButton=false;
        this.checkinButton=true;
        this.checkoutButton=false;
        if(!this.message)
          this.message="You don't have any reservation!";
      }
      else if(this.status==="1"){//if reserved
        this.reserveButton=false;
        this.releaseReserveButton=true;
        this.checkinButton=true;
        this.checkoutButton=false;
        if(!this.message)
          this.message="You have reservation";
      }
      else if(this.status==="2"){//if checked-in
        this.reserveButton=false;
        this.releaseReserveButton=false;
        this.checkinButton=false;
        this.checkoutButton=true;
        if(!this.message)
          this.message="You have been check-in!";
      }
      console.log(this.message)
      loading.dismissAll();
    }, (err) => {
      //Connection failed message
      this.presentToast("Connection issue, please retry later!",10000);
      loading.dismissAll();
    });
  }

  reserve(){
    console.log("go reserve BuildingsPage")
    this.gotopage(BuildingsPage,null)
  }

  unreserve(){
    console.log("go unreserve")
    this.gotopage(UnreservePage,null)
  }

  checkin(){
    console.log("go checkin")
    this.gotopage(CheckinPage,null)
  }

  checkout(){
    console.log("go checkin")
    this.gotopage(CheckoutPage,null)
  }

  gotopage(page,message){
    if(message){ 
      this.navCtrl.push(page, {
        message: message
      });
    }
    else{
      this.navCtrl.push(page);
    }
  }  

  presentToast(msg,duration) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: 'bottom',
      dismissOnPageChange: true,
      cssClass: 'mytoast'
    });
    toast.present();
  }

}
