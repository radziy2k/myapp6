import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FindPeoplePage } from './find';

@NgModule({
  declarations: [
    FindPeoplePage,
  ],
  imports: [
    IonicPageModule.forChild(FindPeoplePage),
  ],
})
export class FindPageModule {}
