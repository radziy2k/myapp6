import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FindPeopleDetailPage } from './people';

@NgModule({
  declarations: [
    FindPeopleDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(FindPeopleDetailPage),
  ],
})
export class PeoplePageModule {}
