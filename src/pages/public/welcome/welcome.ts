import { Component,ViewChild } from '@angular/core';
import { NavController,MenuController,Slides } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { StatusBar } from '@ionic-native/status-bar';

@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {
  @ViewChild('slides') slides: Slides;
 constructor(public navCtrl: NavController,statusBar: StatusBar,public menuCtrl: MenuController) {
       this.menuCtrl.enable(false, 'menu');
  statusBar.backgroundColorByHexString("#2b8a63");
  }

  login(){
  this.navCtrl.push(LoginPage);
  }

  next() {
    this.slides.slideNext();
  }

  prev() {
    this.slides.slidePrev();
  }

}
