import { Component, HostListener, Output, EventEmitter, ElementRef, Input } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,MenuController,LoadingController } from 'ionic-angular';
import { HomePage } from '../../private/home/home';
import {ApiProvider} from "../../../providers/api/api";
import {GloballyProvider} from "../../../providers/globally/globally";
import { Keyboard } from '@ionic-native/keyboard';
// import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  @Output() input: EventEmitter<string> = new EventEmitter<string>();
  @Input('br-data-dependency') nextIonInputId: any = null;

  resposeData : any;
  userData = {"username":"", "password":"",};
  message = null

  constructor(public navCtrl: NavController, 
              public menuCtrl: MenuController,
              public api: ApiProvider,
              private toastCtrl:ToastController,
              public navParams: NavParams,
              public g:GloballyProvider,
              public Keyboard: Keyboard,
              public elementRef: ElementRef,
              public loadingController: LoadingController
              // private storage: Storage
              // public plt:Platform
              ) {
    this.menuCtrl.enable(false, 'menu');
    // this.plt.ready().then(()=>{ plt.registerBackButtonAction(() => { }); });
    this.message = navParams.get('message')
    console.log("login message=",this.message)
  }

  ionViewDidLoad() {
    if(this.message)
      this.presentToast(this.message,20000);
    console.log('ionViewDidLoad Login');
  }

  doLogin(){
    if(this.userData.username && this.userData.password){
      let loading = this.loadingController.create({content : "Logging in ,please wait..."});
      loading.present();
      this.api.authenticate(this.userData.username,this.userData.password).then((result) =>{
        this.resposeData = result;
        // console.log("login:-");
        // console.log(this.resposeData.body);
        if(this.resposeData.body.fullname){
          let userData = {"id":this.resposeData.body.id,"username":this.resposeData.body.username,"fullname":this.resposeData.body.fullname,"contact":this.resposeData.body.contact,"token":this.resposeData.body.token}
          // console.log("successs login");
          localStorage.setItem('userData', JSON.stringify(userData) )
          // this.storage.set('userData', userData);
          this.navCtrl.push(HomePage);
          loading.dismissAll();
        }
        else{
          this.presentToast("Please give valid username and password",10000);
          loading.dismissAll();
        }

      }, (err) => {
        //Connection failed message
        this.presentToast("Connection issue, please retry later!",10000);
        loading.dismissAll();
      });
    }
    else{
      this.presentToast("Please fill in username and password!",5000);
    }
  }

  presentToast(msg,duration) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: 'bottom',
      dismissOnPageChange: true,
      cssClass: 'mytoast'
    });
    toast.present();
  }

  // @HostListener('keydown', ['$event'])
  // keyEvent(event) {
  //   if (event.srcElement.tagName !== "INPUT") {
  //     return;
  //   }

  //   var code = event.keyCode || event.which;
  //   if (code === TAB_KEY_CODE) {
  //     event.preventDefault();
  //     this.onNext();
  //     let previousIonElementValue = this.elementRef.nativeElement.children[0].value;
  //     this.input.emit(previousIonElementValue)
  //   } else if (code === ENTER_KEY_CODE) {
  //     event.preventDefault();
  //     this.onEnter();
  //     let previousIonElementValue = this.elementRef.nativeElement.children[0].value;
  //     this.input.emit(previousIonElementValue)
  //   }
  // }

  // onEnter() {
  //   console.log("onEnter()");
  //   if (!this.nextIonInputId) {
  //     return;
  //   }

  //   let nextInputElement = document.getElementById(this.nextIonInputId);

  //   // On enter, go to next input field
  //   if (nextInputElement && nextInputElement.children[0]) {
  //     let element: any = nextInputElement.children[0];
  //     if (element.tagName === "INPUT") {
  //       element.focus();
  //     }
  //   }
  // }

  // onNext() {
  //   console.log("onNext()");
  //   if (!this.nextIonInputId) {
  //     return;
  //   }

  //   let nextInputElement = document.getElementById(this.nextIonInputId);

  //   // On enter, go to next input field
  //   if (nextInputElement && nextInputElement.children[0]) {
  //     let element: any = nextInputElement.children[0];
  //     if (element.tagName === "INPUT") {
  //       element.focus();
  //     }
  //   }
  // }
}

const TAB_KEY_CODE = 9;
const ENTER_KEY_CODE = 13;
